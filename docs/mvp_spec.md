Spec
---

- Feature
  - UniqueId (required - assigned on creation)
  - CreationTime (required - string - ISO8601)
  - CreatingUser (required - string - username)
  - RoadType (optional - defined text field - see below)
    - InterstateOrMotorway
      - RoadNumber (optional - integer)
      - Direction (optional - string - defined text field - see below) (??)
        - Northbound
        - Eastbound
        - Southbound
        - Westbound
    - HighwayOrTrunk
      - RoadNumber (optional - integer )
      - Direction (optional - string - defined text field - see below) (??)
        - Northbound
        - Eastbound
        - Southbound
        - Westbound
    - Street
      - Name (optional - free text)
  - Geometry (required - calculated from drawn line)
    - Type (defined text field - see below)
      - Point
      - LineString
    - Coordinates (defined text field - see [GEOJSON spec](https://tools.ietf.org/html/rfc7946#section-3.1))
  - Deleted (boolean - default False)
  - Source (optional - string - free text)
  - WikiURL (option - string - free text)
  - Notes (optional - free text field)
  - FeatureType (required - depends on Geometry.Type - defined text field - see below)
    - Point
      - Sign
        - Subtype (optional - defined text field - see below)
          - Regulatory
          - Warning
          - Guide
          - GeneralInformation
          - Temporary
          - School
          - Rail
          - Bicycle
          - Miscellaneous
          - MileMarker
          - Deliniator
        - MUTCDCode (optional - defined text field - dependent on Subtype - see the MUTCD.csv)
        - SignText (optional - free text field)
        - SignPlacement (optional - defined text field)
          - Left
          - Right
          - Overhead
      - Terminal
        - Subtype (optional - defined text field - see below)
          - EndTerminal
          - CrashCushion
          - BluntEnd
          - Other
        - Device (defined text field - dependent on Subtype - see devices.csv)
      - Crash
        - Subtype (optional - defined text field - see below)
          - Fatal
          - InjuryOnly
          - PropertyDamageOnly
        - CrashDate (optional - string ISO8601)
        - TotalFatalities (optional - int)
      - Signal
        - Subtype (optional - defined text field - see below)
          - TrafficSignal
          - PedestrianSignal
          - Other
      - Camera
      - AccessPoint
      - RoadSurfaceMarking
        - Subtype (optional - defined text field - see below)
          - ArrowLeft   
          - ArrowRight
          - ArrowSplitLeftOrStraight
          - ArrowSplitRightOrStraight
          - ArrowStraight
          - CrosswalkZebra
          - GiveWayRow
          - GiveWaySingle
          - StopLine
          - SymbolBicycle
          - Text
            - MarkingText (optional - free text string)
          - Other
      - RoadSurfaceFeature
        - Subtype
          - SpeedBump
      - Other
        - FeatureName (required - free text field)
        - FeatureDescription (optional - free text field)
    - LineString
      - VehicleBarrier
        - Subtype (optional - defined text field - see below)
          - WBeam
          - ThriBeam
          - BoxBeam
          - Cable
          - Concrete
          - Transition
          - BridgeRail
          - Other
        - Device (optional - defined text field - dependent on subtype - see devices.csv)
        - Length (required - float - measured from line coordinates)
      - Fence
        - Subtype (optional - defined text field - see below)
          - SoundBarrier
          - PedestrianFence
          - GameFence
        - Length (required - float - measured from line coordinates)
      - RoadSurfaceMarking
        - Subtype (optional - defined text field - see below)
          - SingleDotted
          - DoubleSolid
          - HalfDotted
          - Other
        - Length (required - float - measured from line coordinates)
      - RoadSurfaceFeature
        - Subtype
          - RumbleStrip
          - PavedShoulder
          - GravelShoulder
          - Other
      - Other
        - FeatureName (required - free text field)
        - FeatureDescription (optional - free text field)
        
- Flag
  - UniqueId (assigned on creation)
  - CreationTime (string - ISO8601)
  - CreatingUser (string - username)
  - Type (string - defined text field - see below)
    - Damage
      - Feature UniqueId (required)
      - Subtype
        - MinorFunctional
        - MajorDysfunctional
    - MisInstallation
      - Feature UniqueId (required)
      - Subtype
        - Assembly
        - Placement
    - UnprotectedHazard
      - Subtype
        - FixedObjectInClearZone
        - SteepSlopeOrDropOff
    - RoadDamage
      - Subtype
        - PotholeMinor
        - PotholeSevere
        - UnmarkedRoad
        - Flooding
        - EdgeDrop
    - AnimalCarcass
    - GarbageRefuseLitter
    - NaturalDebris
    - VehicleDebris
    - SpillPollutant
    - Other
  - Notes (string - free text)
  - Geometry (separate from the feature)
    - Type
      - Point
    - Coordinates (defined text field - see [GEOJSON spec](https://tools.ietf.org/html/rfc7946#section-3.1))
  - Deleted (boolean - default False)

- ChangeLog
  - Date
  - UserId
  - EntryBefore
  - EntryAfter

Resources
---

Where possible we have taken our spec from industry standard sources, including [GeoJSON](https://tools.ietf.org/html/rfc7946#section-3.1), [MUTCD](https://mutcd.fhwa.dot.gov/services/publications/fhwaop02084/index.htm), [Open Street Maps](https://wiki.openstreetmap.org/wiki/Map_features), [Task Force 13](https://tf13.org/Guides/), [Mapillary](https://www.mapillary.com/developer/api-documentation/#complementary)

We also take inspiration from a number of public issue reporting sites, including the [New Jersey DOT](https://www.njdotproblemreporting.com/) and the [Utah DOT](https://www.udot.utah.gov/connect/public/contact-udot/)

- UDOT
  - Issue
    + Guardrail
    + Camera Outage
    + Interstate Lighting
    + Other
    + Pothole
    + Ramp Meters
    + Road Debris/Rockfall
    + Roadway Flood/Drainage/Erosion
    + Rough Road
    + Signage
    + Snow Removal
    + Spills and Pollutant Discharges
    + Striping
    + Traffic Signals
    + Trash/Litter
    + Animal Carcass
    + Fencing
    + Graffiti
    + Vegetation
  - Location
  - Direction of Travel
  - Comment

- NJDOT
  - Location
  - Direction of Travel
  - Issue
    - Advertising/Campaign Sign
    - Animal Carcass/Grass Area, Median, Shoulder
    - Animal Carcass/Roadway
    - Asphalt/Pavement Lifting On Bridge, road, ramp, shldr
    - Break in/NJDOT Property - Maintenance
    - Bridge Navigation Lights Out - Maintenance
    - Bridge Signals Flashing/Not Flashing - Maintenance
    - Bridge Stuck Open/Closed - Maintenance
    - Bump in Road, bridge, ramp, shldr
    - Center Barrier Curb Damage/Misaligned
    - Clogged Storm Drain/Flooding
    - Cracked/Exposed Rebar/Tension Cable on Bridge
    - Cracks in Road, Bridge, Ramp, Shldr
    - Dead Deer/Grass, Median Area, Shldr
    - Dead Deer/Roadway, Ramp
    - Dead/Overgrown Tree
    - Debris in Lane, Ramp, Bridge, Shldr
    - Debris on Shoulder - No Emergency
    - Drums on Road/Pavement
    - Faded Sign
    - Fence Damage in Road or Barrier
    - Fence Damage/Median Barrier
    - Fire
    - Flooding
    - Fuel Spill or Leak/Road, Ramp, Bridge, Shldr
    - Graffiti/Sign Bridge or Road
    - Guide Rail Damage
    - Guiderail Damage in Road
    - Icing Conditions
    - Improper Use of State Vehicle/Equipment - Electrical
    - Improper Use of State Vehicle/Equipment - Maintenance
    - Lines Need Painting/Faded
    - Litter/Garbage
    - Manhole Cover Damaged, Missing, Sticking Up
    - MVA FATAL/Sand/Arrow Board/Safety/Clean Up
    - MVA/Sand/Arrow Board/Safety/Clean Up
    - Other Emergency - Maintenance
    - Other Non Emergency - Maintenance
    - OTTT/Road, Ramp or Shldr Closed
    - Overgrown Grass/Bushes Blocking Visibility
    - Overhead Sign Hanging/Loose - Maintenance
    - Pothole - Non Emergency
    - Pothole on Road, Ramp, Bridge Shldr
    - Road Needs Sweeping/Non-MVA
    - Sign Bent in Traffic/Walkway
    - Sign Down Non Emergency, Shoulder/Median
    - Sign Down on Road/Ramp/Jughandle
    - Sign Missing/Hanging 1 Bolt Non Emergency
    - Sign Missing/Post Sticking Up
    - Sign/Do Not Enter/Any Problems
    - Sign/Keep Right/Any Problems
    - Sign/No Left &amp; Right Turn/Any Problems
    - Sign/No Turn or No U Turn/Any Problems
    - Sign/One Way/Wrong Way/Any Problems
    - Sign/Stop/Any Problems
    - Sign/Yield/Any Problems
    - Sink Hole
    - Slippery Road/Unknown Reason
    - Snow Plow Blocked Driveway, Mail Box
    - Spill - Unknown Type
    - Storm Drain Cover Damaged/Missing/Sticking Up
    - Street Light Out
    - Tree Cracked/Ready to Fall
    - Tree Down Across Road, Ramp, Shldr
    - Tree Down On Grass Area &amp; Off Roadway
    - Tree Limb Blocking Sign or Visibility
    - Tree Limb Down on Shoulder
    - Tree Limb Hanging in Traffic
    - Tree on Wires
    - Utility Pole - Emergency
    - Utility Pole-Non Emergency
    - Vandalism/Njdot Property - Maintenance
    - Wash Out of Road Surface
    - Water Main Break/Damage to Road
    - Water Main Break/Need Safety
    - Water Main Break/Need Salt for Icing
    - Wires Down in Road, Ramp, Shldr

- Relevant [Open Street Map Tags](https://wiki.openstreetmap.org/wiki/Map_features)
  + barrier=cable_barrier
  + barrier=fence
  + barrier=guard_rail
  + barrier=retaining_wall
  + barrier=jersey_barrier
  + barrier=wall
  + highway=street_lamp
  + highway=traffic_signals
  + highway=stop
  + highway=yield
  + highway=traffic_mirror
  + highway=speed_camera
          
