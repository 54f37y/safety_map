Links and resources
---

Load .geojson files 
- https://docs.mapbox.com/mapbox-gl-js/example/cluster/
- https://docs.mapbox.com/mapbox-gl-js/example/geojson-markers/

Cluster the data 
- https://docs.mapbox.com/mapbox-gl-js/api/sources/#geojsonsource#setdata

Write .geojson files 
- https://github.com/geoman-io/leaflet-geoman
- https://github.com/uber/nebula.gl

Show the Mapillary images at a given location 
- https://www.mapillary.com/mapillaryjs
- Mapillary https://bl.ocks.org/oscarlorentzon/0b7c5763225029268fce0324af2b2b3a

Storing all these files
- https://postgis.net/features/
- https://stevebennett.me/2018/05/15/you-might-not-need-postgis-streamlined-vector-tile-processing-for-big-map-visualisations/
- http://hire.stevebennett.me/

