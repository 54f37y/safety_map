How to Contribute
===

At this early stage of the project Safe7y is not looking for community support in developing this project. 

Please check back soon and visit [our website](www.safe7y.com) to find other ways of contributing.