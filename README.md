Overview
===

The goal of [Safety in Numbers (Safe7y)](www.safe7y.com) is to use big data to save lives on roads around the world. We do this by creating the missing map of road safety hardware, including signs, guardrails, and rumble strips. This map is essential for road owners to manage their assets and fix damage and installation problem. It is also essential for equipment manufacturers and researchers to understand and improve the performance of devices and road safety standards.

This project is a web based map and editor which anyone can use to view our data, add missing hardware or road features, and identify issues with existing features. Features marked remain public and are shared with [OpenStreetMap](www.openstreetmap.org) 

Details on a variety of road safety devices and potential issues can be found on our [wiki](wiki.safe7y.com) 


Technical Details
===

Our Stack
---

- GEOJSON - Data format
- Javascript - Client side scripting
- WebGL - Rendering
- Node.js - Package management 
- HTML - Page content
- CSS - Page style
- Django - User authentication and private dataset management
- nginx - Web server
- Certbot - SSL certificate provider
- Python: Server side scripting


Partners
---

- Open Street Map
- Mapbox
- Mapillary
- Kartaview
- AWS


Open Source Resources
---

- https://github.com/umap-project/umap
- https://github.com/openstreetmap/iD
- https://github.com/mapillary/mapillary-js
- https://github.com/mapbox/mapbox-gl-js
- https://github.com/mapbox/geojson.io
- https://github.com/mapbox/csv2geojson
